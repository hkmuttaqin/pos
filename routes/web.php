<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'frontpage\PageController@index');
Route::get('/contact', 'frontpage\PageController@contact');
Route::get('/products', 'frontpage\ProductController@index');
Route::get('/products/{id}', 'frontpage\ProductController@show');

