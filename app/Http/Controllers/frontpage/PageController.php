<?php

namespace App\Http\Controllers\frontpage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function index(){
        return view('frontpage.index');
    }

    public function contact(){
        return view('frontpage.contact');
    }
}
