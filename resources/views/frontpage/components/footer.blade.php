<!-- Footer Section Begin -->
<footer class="footer spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12 col-sm-12">
                <div class="footer__about">
                    <div class="footer__about__logo">
                        <a href="./index.html"><img src="{{ asset('vendor/ogani') }}/img/logo.png" alt=""></a>
                    </div>
                    <ul>
                        <li>Alamat: Desa Sangkanhurip, Kecamatan Katapang, Kabupaten Bandung </li>
                        <li>Telepon: 085722552569</li>
                        <li>Email: cowboy.programmer.id@gmail.com</li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-4">
                <div class="footer__widget">
                    <div class="footer__widget__social">
                        <a href="https://www.facebook.com/hannif.k.muttaqin/"><i class="fa fa-facebook"></i></a>
                        <a href="https://www.instagram.com/cowboyprogrammer/"><i class="fa fa-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UCOTdfJ8dW7K23hxfBONxGMg"><i class="fa fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="footer__copyright">
                    <div class="footer__copyright__text">
                        <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script> Cowboy Programmer All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- Footer Section End -->
