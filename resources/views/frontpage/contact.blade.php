@extends('frontpage.layouts.master')

@push('title')
    - Kontak
@endpush

@push('styles')
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/style.css" type="text/css">
@endpush

@section('content')
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Kontak</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Contact Section Begin -->
    <section class="contact spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_phone"></span>
                        <h4>Telepon</h4>
                        <p>085722552569</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_pin_alt"></span>
                        <h4>Alamat</h4>
                        <p>Desa Sangkanhurip, Kecamatan Katapang, Kabupaten Bandung</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_clock_alt"></span>
                        <h4>Jam Operasional</h4>
                        <p>09:00 WIB - 16:00 WIB </p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 text-center">
                    <div class="contact__widget">
                        <span class="icon_mail_alt"></span>
                        <h4>Email</h4>
                        <p>cowboy.programmer.id@gmail.com</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Section End -->

    <!-- Map Begin -->
    <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15840.284749086586!2d107.5740897!3d-7.0008993!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x3fc9157f74855daf!2sGenetic%20Wear%20Workshop!5e0!3m2!1sid!2sid!4v1589560944987!5m2!1sid!2sid" width="100%" height="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
    </div>
    <!-- Map End -->
@endsection


@push('scripts')
    <script src="{{ asset('vendor/ogani') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery.nice-select.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery-ui.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery.slicknav.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/mixitup.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/main.js"></script>
    
    <script>
        $("#contact").addClass('active')
    </script>
@endpush