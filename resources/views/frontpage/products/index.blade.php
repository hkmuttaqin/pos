@extends('frontpage.layouts.master')

@push('title')
    - Produk
@endpush

@push('styles')
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/style.css" type="text/css">
@endpush

@section('content')
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Produk Cowboy Prograrammer</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-5">
                    <div class="sidebar">
                        <div class="sidebar__item">
                            <h4>Kategori Produk</h4>
                            <ul>
                                <li><a href="#">Kategori Produk 1</a></li>
                                <li><a href="#">Kategori Produk 2</a></li>
                                <li><a href="#">Kategori Produk 3</a></li>
                                <li><a href="#">Kategori Produk 4</a></li>
                            </ul>
                        </div>
                        <div class="sidebar__item">
                            <h4>Harga</h4>
                            <div class="price-range-wrap">
                                <div class="price-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"
                                    data-min="10" data-max="540">
                                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                </div>
                                <div class="range-slider">
                                    <div class="price-input">
                                        <input type="text" id="minamount">
                                        <input type="text" id="maxamount">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sidebar__item">
                            <div class="latest-product__text">
                                <h4>Produk Terbaru</h4>
                                <div class="latest-product__slider owl-carousel">
                                    <div class="latest-prdouct__slider__item">
                                        <a href="{{ url('./products/1') }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('vendor/ogani') }}/img/latest-product/lp-1.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                        <a href="{{ url('./products/1') }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('vendor/ogani') }}/img/latest-product/lp-2.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                        <a href="{{ url('./products/1') }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('vendor/ogani') }}/img/latest-product/lp-3.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="latest-prdouct__slider__item">
                                        <a href="{{ url('./products/1') }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('vendor/ogani') }}/img/latest-product/lp-1.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                        <a href="{{ url('./products/1') }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('vendor/ogani') }}/img/latest-product/lp-2.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                        <a href="{{ url('./products/1') }}" class="latest-product__item">
                                            <div class="latest-product__item__pic">
                                                <img src="{{ asset('vendor/ogani') }}/img/latest-product/lp-3.jpg" alt="">
                                            </div>
                                            <div class="latest-product__item__text">
                                                <h6>Crab Pool Security</h6>
                                                <span>$30.00</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-9 col-md-7">
                    <div class="product__discount">
                        <div class="section-title product__discount__title">
                            <h2>Produk Pilihan</h2>
                        </div>
                        <div class="row">
                            <div class="product__discount__slider owl-carousel">
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{ asset('vendor/ogani') }}/img/product/discount/pd-1.jpg">
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="{{ url('./products/1') }}">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{ asset('vendor/ogani') }}/img/product/discount/pd-2.jpg">
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Vegetables</span>
                                            <h5><a href="{{ url('./products/1') }}">Vegetables’package</a></h5>
                                            <div class="product__item__price">$30.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{ asset('vendor/ogani') }}/img/product/discount/pd-3.jpg">
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="{{ url('./products/1') }}">Mixed Fruitss</a></h5>
                                            <div class="product__item__price">$30.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{ asset('vendor/ogani') }}/img/product/discount/pd-4.jpg">
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="{{ url('./products/1') }}">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{ asset('vendor/ogani') }}/img/product/discount/pd-5.jpg">
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="{{ url('./products/1') }}">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="product__discount__item">
                                        <div class="product__discount__item__pic set-bg"
                                            data-setbg="{{ asset('vendor/ogani') }}/img/product/discount/pd-6.jpg">
                                        </div>
                                        <div class="product__discount__item__text">
                                            <span>Dried Fruit</span>
                                            <h5><a href="{{ url('./products/1') }}">Raisin’n’nuts</a></h5>
                                            <div class="product__item__price">$30.00</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="filter__item">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="filter__sort">
                                    <span>Urutkan Berdasarkan</span>
                                    <select>
                                        <option value="0">Standard</option>
                                        <option value="0">Harga Termurah</option>
                                        <option value="0">Harga Termahal</option>
                                        <option value="0">Produk Terbaru</option>
                                        <option value="0">Produk Terlama</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="filter__found text-right">
                                    <h6><span>16</span> Produk Ditemukan</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-1.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-2.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-3.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-4.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-5.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-6.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-7.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-8.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-9.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-10.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-11.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <div class="product__item">
                                <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-12.jpg"></div>
                                <div class="product__item__text">
                                    <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                                    <h5>$30.00</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="product__pagination text-center">
                        <a href="#">1</a>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#"><i class="fa fa-long-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Section End -->
@endsection


@push('scripts')
    <script src="{{ asset('vendor/ogani') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery.nice-select.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery-ui.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery.slicknav.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/mixitup.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/main.js"></script>
    
    <script>
        $("#product").addClass('active')
    </script>
@endpush