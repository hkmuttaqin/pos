@extends('frontpage.layouts.master')

@push('title')
    - Produk
@endpush

@push('styles')
    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/font-awesome.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/elegant-icons.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/nice-select.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/jquery-ui.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/owl.carousel.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/slicknav.min.css" type="text/css">
    <link rel="stylesheet" href="{{ asset('vendor/ogani') }}/css/style.css" type="text/css">
@endpush

@section('content')
    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/breadcrumb.jpg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2>Produk 1</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Product Details Section Begin -->
    <section class="product-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__pic">
                        <div class="product__details__pic__item">
                            <img class="product__details__pic__item--large"
                                src="{{ asset('vendor/ogani') }}/img/product/details/product-details-1.jpg" alt="">
                        </div>
                        <div class="product__details__pic__slider owl-carousel">
                            <img data-imgbigurl="{{ asset('vendor/ogani') }}/img/product/details/product-details-2.jpg"
                                src="{{ asset('vendor/ogani') }}/img/product/details/thumb-1.jpg" alt="">
                            <img data-imgbigurl="{{ asset('vendor/ogani') }}/img/product/details/product-details-3.jpg"
                                src="{{ asset('vendor/ogani') }}/img/product/details/thumb-2.jpg" alt="">
                            <img data-imgbigurl="{{ asset('vendor/ogani') }}/img/product/details/product-details-5.jpg"
                                src="{{ asset('vendor/ogani') }}/img/product/details/thumb-3.jpg" alt="">
                            <img data-imgbigurl="{{ asset('vendor/ogani') }}/img/product/details/product-details-4.jpg"
                                src="{{ asset('vendor/ogani') }}/img/product/details/thumb-4.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3>Produk 1</h3>
                        <div class="product__details__price">$50.00</div>
                        <p>Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vestibulum ac diam sit amet quam
                            vehicula elementum sed sit amet dui. Sed porttitor lectus nibh. Vestibulum ac diam sit amet
                            quam vehicula elementum sed sit amet dui. Proin eget tortor risus.</p>
                        <a href="#" class="primary-btn">Tokopedia</a>
                        <a href="#" class="primary-btn">Shopee</a>
                        <a href="#" class="primary-btn">Bukalapak</a>
                        <a href="#" class="primary-btn">Lazada</a>
                        <ul>
                            <li><a href="#" class="primary-btn"><i class="fa fa-whatsapp"></i> Tanya Tentang Produk Ini</a></li>
                            <li><b>Berat</b> <span>500 gram</span></li>
                            <li><b>Bagikan Produk Ini</b>
                                <div class="share">
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-instagram"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                    aria-selected="true">Deskripsi</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                    aria-selected="false">Informasi</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Deskripsi Produk</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus. Vivamus
                                        suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam sit amet quam
                                        vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat,
                                        accumsan id imperdiet et, porttitor at sem. Praesent sapien massa, convallis a
                                        pellentesque nec, egestas non nisi. Vestibulum ac diam sit amet quam vehicula
                                        elementum sed sit amet dui. Vestibulum ante ipsum primis in faucibus orci luctus
                                        et ultrices posuere cubilia Curae; Donec velit neque, auctor sit amet aliquam
                                        vel, ullamcorper sit amet ligula. Proin eget tortor risus.</p>
                                        <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                        elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                        porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                        nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.
                                        Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Sed
                                        porttitor lectus nibh. Vestibulum ac diam sit amet quam vehicula elementum
                                        sed sit amet dui. Proin eget tortor risus.</p>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Informasi Produk</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                    <p>Praesent sapien massa, convallis a pellentesque nec, egestas non nisi. Lorem
                                        ipsum dolor sit amet, consectetur adipiscing elit. Mauris blandit aliquet
                                        elit, eget tincidunt nibh pulvinar a. Cras ultricies ligula sed magna dictum
                                        porta. Cras ultricies ligula sed magna dictum porta. Sed porttitor lectus
                                        nibh. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->

    <!-- Related Product Section Begin -->
    <section class="related-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title related__product__title">
                        <h2>Produk Terkait</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-1.jpg"></div>
                        <div class="product__item__text">
                            <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-2.jpg"></div>
                        <div class="product__item__text">
                            <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-3.jpg"></div>
                        <div class="product__item__text">
                            <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <div class="product__item">
                        <div class="product__item__pic set-bg" data-setbg="{{ asset('vendor/ogani') }}/img/product/product-7.jpg"></div>
                        <div class="product__item__text">
                            <h6><a href="{{ url('./products/1') }}">Crab Pool Security</a></h6>
                            <h5>$30.00</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Related Product Section End -->
@endsection


@push('scripts')
    <script src="{{ asset('vendor/ogani') }}/js/jquery-3.3.1.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/bootstrap.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery.nice-select.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery-ui.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/jquery.slicknav.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/mixitup.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/owl.carousel.min.js"></script>
    <script src="{{ asset('vendor/ogani') }}/js/main.js"></script>
    
    <script>
        $("#product").addClass('active')
    </script>
@endpush